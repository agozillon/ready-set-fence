using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using gamelib;
using System.IO;
using System.IO.IsolatedStorage;

namespace readySetFence
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics; // initilizing a Graphics Device Manager Class
        SpriteBatch spriteBatch; // initilizing a SpriteBatch class

        // Resolution
        int displayWidth;
        int displayHeight;

        SpriteFont mainFont;        // Font for drawing text on the screen

        Boolean gameOver = false;   // Is the game over TRUE or FALSE?    
        Boolean upped = false;  // boolean to cancel out points going up constantly
        Boolean turnEnd = false; // used to signify when end turn gets called
        Boolean pointUp = true; // also to stop continuous point adding
        Boolean player2RoundWinBool = false; // boolean used to signify the end of round 
        Boolean player1RoundWinBool = false; // boolean used to signify the end of round
        Boolean movesFinished = true; // bool for signifying the end of a player choosing moves
        Boolean soundOnBool = true; // bool for signifying that the sound is on
        Boolean drawBool = false; //bool for signifying if a draw occurs
        Boolean player1GameWin = false; // bool for signifying if the game has been won yet
        Boolean player2GameWin = false; // bool for signifying if the game has been won yet
        Boolean player1StreakEnd = false; // bool for the winstreak mode when player 1 loses to end the streak
        Boolean player2StreakEnd = false; // bool for the winstreak mode when player 2 loses to end the streak
        Boolean winStreakMode = false; // bool for saying that winStreakMode is active, it's defaulted to false. 
     
        float gameRunTime = 0;      // Time since game started

        SoundEffect roundChange, menuSelect, moveSelect, musicTrack; // initilizing sound effect variables
        SoundEffectInstance music; // initializing a sound instance for a music track

        graphic2d tutorialPage, regularBg;       // Background image

        

        GamePadState[] pad = new GamePadState[1];       // Array to hold gamepad states, in this case gamepad is the phone
        sprite2d endTurn, player1RoundWin, player2RoundWin, soundOff, soundOn, deleteButton, draw, player1WinScreen, player2WinScreen, exit; // creating various sprite2ds classes
        animatedsprite blueAnimation, redAnimation; // initilizing 2 seperate animatedsprite classes

        const int numberOfOptions = 5;                    // Number of main menu options
        sprite2d[] menuOptions = new sprite2d[numberOfOptions]; // Array of sprites to hold the menu options
        sprite2d[] title = new sprite2d[2]; // array of sprites to hold title graphics
      
        int optionSelected = 0; // Current menu option selected
        int currentChoice = 5; // counter to check how many choices the player has currently made
        int startTimer; // timer to give a delay whilst choosing moves
        int endTimer = 100; //timer to give a delay whilst choosing moves
        int turnValue = 0; //a value that will be used to check whos turn it is
        int player1Score = 0; // int for player1 initial score
        int player2Score = 0; // int for player2 initial score
        int player1RoundsWon = 0; // int for player1 rounds won
        int player2RoundsWon = 0; // int for player2 rounds won
        int gameState = -1;         // Current game state

        // initializing all my checkround timers so I can later get the animations to play on time
        int bigTimerStart = 0;
        const int numberOfAnimationTimers = 7;
        int[] animationTimer = new int[numberOfAnimationTimers];

        const int numberOfHighscores = 10;                              // Number of high scores to store
        int[] highscores = new int[numberOfHighscores];                 // Array of high scores
        string[] highscoreNames = new string[numberOfHighscores];       // Array of high score names
        public int[] player1Choices = new int[numberOfChoices];         // Array of integers to save values for player1
        public int[] player2Choices = new int[numberOfChoices];         // Array of integers to save values for player2

        // Main 3D Game Camera
        camera gameCamera;

        staticmesh ground;  // 3D graphic for the ground in-game

        const int numberOfMoves = 5; // initilizing a const int for my array of moves
        const int numberOfChoices = 6; // initilizing a const int for my array of choices and moves
        staticmesh[,] choices = new staticmesh[numberOfChoices, numberOfMoves];  // initilizing an array using choices, moves to hold my 3d choices
        staticmesh[] moves = new staticmesh[numberOfMoves];     // an array of 3d models to hold the 3d buttons for my moves

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromTicks(333333);

            // Extend battery life under lock.
            InactiveSleepTime = TimeSpan.FromSeconds(1);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            displayWidth = graphics.GraphicsDevice.Viewport.Width;
            displayHeight = graphics.GraphicsDevice.Viewport.Height;
            graphics.ToggleFullScreen();
        
            gameCamera = new camera(new Vector3(0, 579, 0), new Vector3(0, 0, 0), displayWidth, displayHeight, 45, Vector3.Left, 579, 20000);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            mainFont = Content.Load<SpriteFont>("quartz4");  // Load the quartz4 font
            
            // setting up animation timers to be 25 apart
            for (int i = 0; i < numberOfAnimationTimers; i++)
            {
                animationTimer[i] = i*25 + 25;
            }
            
            // below I load most of my 2d graphics that aren't backgrounds into the various sprite2d classes I initilized and then set them up
            title[0] = new sprite2d(Content, "fencing", displayWidth / 2, 100, 1, Color.White, true);                                     // title for one of my pages
            title[1] = new sprite2d(Content, "fencinghighscore&options", displayWidth / 2, 100, 1, Color.White, true);                    // title for my highscore/options page
            endTurn = new sprite2d(Content, "endturn", displayWidth / 2, 200, 1, Color.White, true);                                      // the end turn button that comes up 
            player1RoundWin = new sprite2d(Content, "Roundendplayer1wins", displayWidth / 2, displayHeight / 2, 1, Color.White, true);    // screen that comes up to show when player 1 has won a round
            player2RoundWin = new sprite2d(Content, "Roundendplayer2wins", displayWidth / 2, displayHeight / 2, 1, Color.White, true);    // screen that comes up to show when player 2 has won a round
            soundOff = new sprite2d(Content, "soundoff",  100, 400, 0.5f, Color.White, true);                                             // the sound is off button for when the sound is currently muted
            soundOn = new sprite2d(Content, "soundOn", 100, 300, 0.5f, Color.White, true);                                                // the sound is on button for when the sound is currently on
            deleteButton = new sprite2d(Content, "DeleteButton", 760, 330, 0.7f, Color.White, true);                                           // the delete button I use for deleting my currently selected set of moves
            draw = new sprite2d(Content, "roundoverdraw", displayWidth / 2, displayHeight / 2, 1f, Color.White, true);                    // the screen that comes up to show when the 2 players have ended the round on a draw
            player1WinScreen = new sprite2d(Content, "player1gamewin", displayWidth / 2, displayHeight / 2, 1f, Color.White, true);          // shows when player 1 wins the game
            player2WinScreen = new sprite2d(Content, "player2gamewin", displayWidth / 2, displayHeight / 2, 1f, Color.White, true);         // shows when player 2 wins the game
            exit = new sprite2d(Content, "exitbutton",  75, 25, 0.5f, Color.White, true);                                                 // exit button for when the player wants to quit out of the current game mode there in

            // using a static mesh to load in a 3d background and set it up, used as the background for in game
            ground = new staticmesh(Content, "ingamebackground", 1f, new Vector3(0, 0, 0), new Vector3(MathHelper.ToRadians(90), MathHelper.ToRadians(90), MathHelper.ToRadians(180))); 
            
            // loading in and setting up my 2d backgrounds into my graphics2d classes
            tutorialPage = new graphic2d(Content, "Tutorialpage2", displayWidth, displayHeight); // the tutorial page background 
            regularBg = new graphic2d(Content, "normalbg", displayWidth, displayHeight);         // the normal background I use out of game
           

            // below I load and setup my various 2d animations I use within my game
            blueAnimation = new animatedsprite(new Vector3(300, 200, 0), 6);                                                              // using the animated sprite classes constructor and feeding in the information i need 
            blueAnimation.spriteanimation[0] = new animation(Content, "Standingposeblue", 0, 0, 1f, Color.White, true, 3, 1, 3, true, false);             // loading in my Standing pose for the blue character which is the default animation 
            blueAnimation.spriteanimation[1] = new animation(Content, "ParryBlue", 0, 0, 1f, Color.White, false, 5, 5, 1, false, false);                  // loading in my parry for the blue character which is the animation i play when he parrys
            blueAnimation.spriteanimation[2] = new animation(Content, "Thrustanimationblue", 0, 0, 1f, Color.White, false, 5, 5, 1, false, false);        // loading in my thrust for the blue character which is the animation i play when he thrusts
            blueAnimation.spriteanimation[3] = new animation(Content, "Dodgeanimationblue", 0, 0, 1f, Color.White, false, 3, 3, 1, false, false);         // loading in my dodge for the blue character which is the animation i play when he dodges
            blueAnimation.spriteanimation[4] = new animation(Content, "Downwardslashblueanimation", 0, 0, 1f, Color.White, false, 3, 3, 1, false, false); // loading in my downward slash for the blue character which is the animation i play when he dodges
            blueAnimation.spriteanimation[5] = new animation(Content, "upwardslashblueanimation", 0, 0, 1f, Color.White, false, 3, 3, 1, false, false);   // loading in my upward slash for the blue character which is the animation that plays when he upward slashes

            redAnimation = new animatedsprite(new Vector3(500, 200, 0), 6);
            redAnimation.spriteanimation[0] = new animation(Content, "Standingposered", 0, 0, 1f, Color.White, true, 3, 1, 3, true, false);
            redAnimation.spriteanimation[1] = new animation(Content, "ParryRed", 0, 0, 1f, Color.White, false, 5, 5, 1, false, false);      
            redAnimation.spriteanimation[2] = new animation(Content, "Thrustanimationred#", 0, 0, 1f, Color.White, false, 5, 5, 1, false, false); 
            redAnimation.spriteanimation[3] = new animation(Content, "Dodgeanimationred", 0, 0, 1f, Color.White, false, 3, 3, 1, false, false); 
            redAnimation.spriteanimation[4] = new animation(Content, "Downwardslashredanimation", 0, 0, 1f, Color.White, false, 3, 3, 1, false, true); 
            redAnimation.spriteanimation[5] = new animation(Content, "upwardslashredanimation", 0, 0, 1f, Color.White, false, 3, 3, 1, false, true);

            // loading in more 2d graphics as markers for my menu options
            menuOptions[0] = new sprite2d(Content, "LargeMaskPlay", 100, displayHeight / 2, 0.3f, Color.White, true);
            menuOptions[1] = new sprite2d(Content, "LargeMaskTutorial", 300, displayHeight / 2, 0.3f, Color.White, true);
            menuOptions[2] = new sprite2d(Content, "LargeMaskoptions", 500, displayHeight / 2, 0.3f, Color.White, true);
            menuOptions[3] = new sprite2d(Content, "LargeMaskWinStreakMode", 700, displayHeight / 2, 0.3f, Color.White, true);
            menuOptions[4] = new sprite2d(Content, "LargeMaskExit", 400, 400, 0.3f, Color.White, true);

            
            // giving all of my menu options bounding boxes
            for (int i = 0; i < numberOfOptions; i++)
            {
                menuOptions[i].bbox = new BoundingBox(new Vector3(menuOptions[i].position.X - menuOptions[i].rect.Width / 2, menuOptions[i].position.Y - menuOptions[i].rect.Height / 2, 0),
                                   new Vector3(menuOptions[i].position.X + menuOptions[i].rect.Width / 2, menuOptions[i].position.Y + menuOptions[i].rect.Height / 2, 0));
            }

            // below I create the bounding boxes for my 2d graphics that are tapable in game
            endTurn.bbox = new BoundingBox(new Vector3(endTurn.position.X - endTurn.rect.Width / 2, endTurn.position.Y - endTurn.rect.Height / 2, 0),
                                   new Vector3(endTurn.position.X + endTurn.rect.Width / 2, endTurn.position.Y + endTurn.rect.Height / 2, 0));

            player1RoundWin.bbox = new BoundingBox(new Vector3(player1RoundWin.position.X - player1RoundWin.rect.Width / 2, player1RoundWin.position.Y - player1RoundWin.rect.Height / 2, 0),
                                   new Vector3(player1RoundWin.position.X + player1RoundWin.rect.Width / 2, player1RoundWin.position.Y + player1RoundWin.rect.Height / 2, 0));

            player2RoundWin.bbox = new BoundingBox(new Vector3(player2RoundWin.position.X - player2RoundWin.rect.Width / 2, player2RoundWin.position.Y - player2RoundWin.rect.Height / 2, 0),
                                   new Vector3(player2RoundWin.position.X + player2RoundWin.rect.Width / 2, player2RoundWin.position.Y + player2RoundWin.rect.Height / 2, 0));

            soundOn.bbox = new BoundingBox(new Vector3(soundOn.position.X - soundOn.rect.Width / 2, soundOn.position.Y - soundOn.rect.Height / 2, 0),
                                   new Vector3(soundOn.position.X + soundOn.rect.Width / 2, soundOn.position.Y + soundOn.rect.Height / 2, 0));

            soundOff.bbox = new BoundingBox(new Vector3(soundOff.position.X - soundOff.rect.Width / 2, soundOff.position.Y - soundOff.rect.Height / 2, 0),
                                   new Vector3(soundOff.position.X + soundOff.rect.Width / 2, soundOff.position.Y + soundOff.rect.Height / 2, 0));

            deleteButton.bbox = new BoundingBox(new Vector3(deleteButton.position.X - deleteButton.rect.Width / 2, deleteButton.position.Y - deleteButton.rect.Height / 2, 0),
                                   new Vector3(deleteButton.position.X + deleteButton.rect.Width / 2, deleteButton.position.Y + deleteButton.rect.Height / 2, 0));

            draw.bbox = new BoundingBox(new Vector3(draw.position.X - draw.rect.Width / 2, draw.position.Y - draw.rect.Height / 2, 0),
                                   new Vector3(draw.position.X + draw.rect.Width / 2, draw.position.Y + draw.rect.Height / 2, 0));

            player1WinScreen.bbox = new BoundingBox(new Vector3(player1WinScreen.position.X - player1WinScreen.rect.Width / 2, player1WinScreen.position.Y - player1WinScreen.rect.Height / 2, 0),
                                  new Vector3(player1WinScreen.position.X + player1WinScreen.rect.Width / 2, player1WinScreen.position.Y + player1WinScreen.rect.Height / 2, 0));

            player2WinScreen.bbox = new BoundingBox(new Vector3(player2WinScreen.position.X - player2WinScreen.rect.Width / 2, player2WinScreen.position.Y - player2WinScreen.rect.Height / 2, 0),
                                  new Vector3(player2WinScreen.position.X + player2WinScreen.rect.Width / 2, player2WinScreen.position.Y + player2WinScreen.rect.Height / 2, 0));

            exit.bbox = new BoundingBox(new Vector3(exit.position.X - exit.rect.Width / 2, exit.position.Y - exit.rect.Height / 2, 0),
                                  new Vector3(exit.position.X + exit.rect.Width / 2, exit.position.Y + exit.rect.Height / 2, 0));


          
            // loading the sounds in
            roundChange = Content.Load<SoundEffect>("Roundbell");         // plays when the round changes
            menuSelect = Content.Load<SoundEffect>("Menu Select Sound");  // plays when you select something on the menu
            moveSelect = Content.Load<SoundEffect>("Moveselectionsound"); // plays when you click a move in game
            musicTrack = Content.Load<SoundEffect>("destination");        // plays as the music track throughout the game

            //creating an instance of the track
            music = musicTrack.CreateInstance();
            music.IsLooped = true; // looping it 
            music.Volume = 0.5f; // volume to 50%

            // loading my 3d buttons and setting them up 
            moves[0] = new staticmesh(Content, "parry", 1f, new Vector3(185, 0, -150), new Vector3(80, 1.57005f, 0));      // parry model
            moves[1] = new staticmesh(Content, "dodge", 1f, new Vector3(185, 0, -300), new Vector3(80, 1.57005f, 0));      // dodge model
            moves[2] = new staticmesh(Content, "thrust", 1f, new Vector3(185, 0, 300), new Vector3(80, 1.57005f, 0));      // thrust model
            moves[3] = new staticmesh(Content, "upwardslash", 1f, new Vector3(185, 0, 150), new Vector3(80, 1.57005f, 0)); // upwardslash model
            moves[4] = new staticmesh(Content, "downwardslash", 1f, new Vector3(185, 0, 0), new Vector3(80, 1.57005f, 0)); // downwardslash model

            // setting the bounding boxes for the move list (the tapable 3d buttons)
            moves[0].bbox = new BoundingBox(new Vector3(500, 390, 0), new Vector3(600, 480, 0)); // Parry bbpx
            moves[1].bbox = new BoundingBox(new Vector3(670, 390, 0), new Vector3(770, 480, 0)); // Dodge bbox
            moves[2].bbox = new BoundingBox(new Vector3(30, 390, 0), new Vector3(140, 480, 0));  // Thrust bbox
            moves[3].bbox = new BoundingBox(new Vector3(190, 390, 0), new Vector3(290, 480, 0)); // upwardslash bbox
            moves[4].bbox = new BoundingBox(new Vector3(350, 390, 0), new Vector3(450, 480, 0)); // downwardslash bbox

            // loading my 3d choices into there array and setting them up(these are not the buttons these are the invisible ones that display what you have chosen) 
            for (int count = 0; count < numberOfChoices; count++)
            {
                choices[count, 0] = new staticmesh(Content, "parry", 1f, new Vector3(95, 0, -250 + (count * 100)), new Vector3(80, 1.57005f, 0)); // setting them up so that they display count * 100 apart on the Z e.g -250 + 2 * 100 = -50
                choices[count, 1] = new staticmesh(Content, "dodge", 1f, new Vector3(95, 0, -250 + (count * 100)), new Vector3(80, 1.57005f, 0));
                choices[count, 2] = new staticmesh(Content, "thrust", 1f, new Vector3(95, 0, -250 + (count * 100)), new Vector3(80, 1.57005f, 0));
                choices[count, 3] = new staticmesh(Content, "upwardslash", 1f, new Vector3(95, 0, -250 + (count * 100)), new Vector3(80, 1.57005f, 0));
                choices[count, 4] = new staticmesh(Content, "downwardslash", 1f, new Vector3(95, 0, -250 + (count * 100)), new Vector3(80, 1.57005f, 0));
            }

            // giving all my choices values so I can match them up later
            for (int count = 0; count < numberOfChoices; count++) 
            {
                choices[count, 0].value = 1;
                choices[count, 1].value = 2;
                choices[count, 2].value = 3;
                choices[count, 3].value = 4;
                choices[count, 4].value = 5;
            }

            // Load High Scores in
            using (IsolatedStorageFile savegamestorage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (savegamestorage.FileExists("highscores.txt"))
                {
                    using (IsolatedStorageFileStream fs = savegamestorage.OpenFile("highscores.txt", System.IO.FileMode.Open))
                    {
                        using (StreamReader sr = new StreamReader(fs))
                        {
                            string line;
                            for (int i = 0; i < numberOfHighscores; i++)
                            {
                                line = sr.ReadLine();
                                highscoreNames[i] = line.Trim();
                                line = sr.ReadLine();
                                highscores[i] = Convert.ToInt32(line);
                            }

                            sr.Close();
                        }
                    }
                }
            }
            // Sort high scores
            Array.Sort(highscores, highscoreNames);
            Array.Reverse(highscores);
            Array.Reverse(highscoreNames);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

       
        public void nextround()
        {
            // function i created that gets called at the end of a round to reset various variables and check the outcome of the round
            
            movesFinished = true; // sets moves finished back to true

            if (player1Score > player2Score) // if player 1 score greater than player 2 score adds a round point and sets boolean to true so that the background shows up for p1 winning the round
            {
                player1RoundWinBool = true;  // player 1 round win boolean set to true to display the background 
                player1RoundsWon += 1;       // player1 points go up need no bool for this to stop the points since this gets looped only once before the points get set back to 0
                
                if (winStreakMode == true)          // if the game state is equal to 3(the winstreak game mode) then player 2 streak end equals true 
                {
                    player2StreakEnd = true; 
                }
            }

            if (player2Score > player1Score) // same as above except when player 2 wins
            {
                player2RoundWinBool = true;
                player2RoundsWon += 1;

                if (winStreakMode == true)
                {
                    player1StreakEnd = true;
                }
            }

            if (player2Score == player1Score) // sets a boolean to true to show the draw screen when both of the players scores are the same
            {
                drawBool = true;
            }

            //resetting all of the values I need to reset back to there original value
            turnValue = 0; 
            currentChoice = 5;
            player1Score = 0;
            player2Score = 0;

            for (int count = 0; count < numberOfChoices; count++)
            {
                player1Choices[count] = 0;
                player2Choices[count] = 0;
            }

            // if the sound is not currently muted(its set to true) then play the roundchange sound
            if(soundOnBool) 
            roundChange.Play();
        }

        public void nextturn()
        {
            // the nextturn function  created which basically just puts moves to false and the choices back to 5 when its the next players turn

            currentChoice = 5;

            for (int count = 0; count < numberOfChoices; count++)
            {
                for (int i = 0; i < numberOfMoves; i++)
                    choices[count, i].visible = false;
            }
      
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here
            pad[0] = GamePad.GetState(PlayerIndex.One);     // Reads gamepad 1 AKA phone emulator

            float timebetweenupdates = (float)gameTime.ElapsedGameTime.TotalMilliseconds; // Time between updates
            gameRunTime += timebetweenupdates;  // Count how long the game has been running for

            // TODO: Add your update logic here
            switch (gameState)
            {
                case -1:
                    // Game is on the main  
                    updatemenu();
                    break;
                case 0:
                    // Game is being played
                    updategame(timebetweenupdates);
                    break;
                case 1:
                    // Options menu
                    updatetutorial();
                    break;
                case 2:
                    // High Score table
                    updatehighscoreandoptions();
                    break;

                case 3:
                    //second game mode(winstreak mode)
                    updategame(timebetweenupdates);
                    break;

                default:
                    // exit if none of the above are pressed

                    // save high scores
                    using (IsolatedStorageFile savegamestorage = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        using (IsolatedStorageFileStream fs = new IsolatedStorageFileStream("highscores.txt", System.IO.FileMode.Create, savegamestorage))
                        {
                            using (StreamWriter writer = new StreamWriter(fs))
                            {
                                for (int i = 0; i < numberOfHighscores; i++)
                                {
                                    writer.WriteLine(highscoreNames[i]);
                                    writer.WriteLine(highscores[i].ToString());
                                }
                                writer.Flush();
                                writer.Close();
                            }
                        }
                    }

                    this.Exit();    // Quit Game
                    break;
            }

            base.Update(gameTime);
        }

        void reset()
        {
            // Reset everything for a new game largely reseting all values back to there initial value

            for (int count = 0; count > numberOfChoices; count++) // setting all of the players choices to 0 (the values used for the choicevschoice function)
            {
                player1Choices[count] = 0;
                player2Choices[count] = 0;
            }


            // setting the booleans i need back to there original values           
            upped = false;               
            turnEnd = false;            
            pointUp = true;              
            player2RoundWinBool = false; 
            player1RoundWinBool = false;   
            movesFinished = true;
            drawBool = false;
            player1GameWin = false;
            player2GameWin = false;
            
            // setting turnvalue to 0 and currentchoice to 5 as well as the animations to there default
            turnValue = 0;
            currentChoice = 5;
            blueAnimation.state = 0;
            redAnimation.state = 0;
        
            if (bigTimerStart > 0) // if the bigtimerstart is greater than 0 reset it back to 0 
              bigTimerStart = 0;

            // setting all of the values to 0
            player1Score = 0;
            player2Score = 0; 
            player1RoundsWon = 0; 
            player2RoundsWon = 0; 

            // setting all of my choices back to invisible
            for (int count = 0; count < numberOfChoices; count++)
            {
                for (int i = 0; i < numberOfMoves; i++)
                    choices[count, i].visible = false;
            }

            gameOver = false;
        }

        public void updatemenu()
        {
            optionSelected = -1;

            // Check for touch over a menu option
            TouchCollection tcoll = TouchPanel.GetState();
            Boolean pressed = false;
            BoundingSphere touchsphere = new BoundingSphere(new Vector3(0, 0, 0), 0);

            foreach (TouchLocation t1 in tcoll)
            {
                if (t1.State == TouchLocationState.Pressed || t1.State == TouchLocationState.Moved)
                {
                    pressed = true;
                    touchsphere = new BoundingSphere(new Vector3(t1.Position.X, t1.Position.Y, 0), 1);
                }
            }

            // checking if the sound bool is true and if it is and the music is stopped then to play it
            if (soundOnBool)
            {
                if (music.State == SoundState.Stopped) music.Play();
            }
            else if (music.State == SoundState.Playing) music.Stop(); // checking if its not true and its playing then to stop it

            for (int i = 0; i < numberOfOptions; i++) // checking all of the menu options and if any of them are clicked then to count it as selected and change the gamestate 
            {
                if (pressed && touchsphere.Intersects(menuOptions[i].bbox))
                { 
                    if (soundOnBool) // if the sound is on play then menu selection sound
                    {
                        menuSelect.Play();
                    }

                    optionSelected = i;
                    gameState = optionSelected;

                    // if gameState is == 3 then set winStreakMode bool to true, else false
                    if (gameState == 3)
                        winStreakMode = true; 
                    else
                        winStreakMode = false;

                    if (gameState == 0 || gameState == 3) reset(); // If play game has been selected reset positions etc
                }
            }
        }

        public void drawmenu()
        {
            spriteBatch.Begin();
            regularBg.drawme(ref spriteBatch); // draw the normal background
       
            // Draw menu options
            for (int i = 0; i < numberOfOptions; i++)
            {
               menuOptions[i].drawme(ref spriteBatch);
            }

            // draw the current title
            title[0].drawme(ref spriteBatch);

            spriteBatch.End();
        }

        public void updategame(float gtime)
        {
            // Main game code
            if (!gameOver && gameState == 0 || !gameOver && gameState == 3) // if game is not over and gamestate == 0 then run this code (this is the normal game code)
            {
                blueAnimation.updatesprite(gtime);   // update all of the animation(blue character) sprites
                redAnimation.updatesprite(gtime); // update all of the animation2(red character) sprites

                
                // Check for touch
                TouchCollection tcoll = TouchPanel.GetState();
                Boolean pressed = false;
                BoundingSphere touchsphere = new BoundingSphere(new Vector3(0, 0, 0), 0);

                foreach (TouchLocation t1 in tcoll)
                {
                    if (t1.State == TouchLocationState.Pressed || t1.State == TouchLocationState.Moved)
                    {
                        pressed = true;
                        touchsphere = new BoundingSphere(new Vector3(t1.Position.X, t1.Position.Y, 0), 1);
                    }
                }

                // Game is being played
                if (pad[0].Buttons.Back == ButtonState.Pressed)
                {
                    //gameState = -1;
                    gameOver = true; // Allow user to quit game
                }
                if (pressed && touchsphere.Intersects(exit.bbox)) // if the exit button is pressed then set gameover to true
                {
                    //gameState = -1;
                    gameOver = true;
                }

                // this is my moves selection code it basically allows you 
                // to click on the moves on screen it then shows what your choice is on screen, 
                //plays a sound and applies a timer so you need to wait a second to click on another
                if (movesFinished) // if moves finished is true
                {
                    for (int i = 0; i < numberOfMoves; i++) // go through the array 
                    {
                        if (!upped && pressed && touchsphere.Intersects(moves[i].bbox)) // see if upped is false and check if the touch sphere intersects a moves bbox
                        {
                            if (soundOnBool) // play the select sound
                            {
                                moveSelect.Play();
                            }

                            choices[currentChoice, i].visible = true; // set a choice to visible based on the current choice and current move in the array
                            currentChoice -= 1;                       // take a point away from current choice
                            upped = true;                             // set upped to true
                            startTimer = 0;                           // set start timer to 0
                        }
                        else
                            startTimer++;                            // since upped is no longer true start the timer
                    }
                }

                if (startTimer > endTimer) // if start timer is greater than end timer then set upped to false
                    upped = false;

                // this is my value setting code 
                for (int count = 0; count < numberOfChoices; count++) // the values save backwards within these loops so playerchoices[5] is the players first choice(just an fyi)
                {
                    for (int i = 0; i < numberOfMoves; i++) 
                    {
                        if (choices[count, i].visible && turnValue == 0) 
                        {
                            player1Choices[count] = choices[count, i].value; // save the visible choices values as player1choices
                        }
                    }
                }

                for (int count = 0; count < numberOfChoices; count++) // the values save backwards within these loops so playerchoices[5] is the players first choice(just an fyi)
                {
                    for (int i = 0; i < numberOfMoves; i++)
                    {
                        if (choices[count, i].visible && turnValue == 1) // if the choice is visible and the turnvalue is 1(second players turn)
                        {
                            player2Choices[count] = choices[count, i].value; // save the visible choices values as player2choices
                        }
                    }
                }

                // this is my delete choices code if the player taps the delete button then all of my choices are set to invisible depending on the turnvalue(whos turn it is) then that players choice values are also set to 0
                if (pressed && !turnEnd && touchsphere.Intersects(deleteButton.bbox))
                {
                    for (int count = 0; count < numberOfChoices; count++)
                    {
                        for (int i = 0; i < numberOfMoves; i++)
                            choices[count, i].visible = false;
                    }

                    if (turnValue == 0) // if turn value is 0 effectivley player 1s turn set there choice values to 0
                    {
                        for (int count = 0; count < numberOfChoices; count++) 
                        {
                            player1Choices[count] = 0;
                        }
                    }

                    if (turnValue == 1) // if turn value is 1 effectivley player 2s turn set there choice values to 0
                    {
                        for (int count = 0; count < numberOfChoices; count++)
                        {
                            player2Choices[count] = 0;
                        }
                    }

                    currentChoice = 5; // and set the current choice back to 5
                }

                // my code to check if it is the end of the current players turn
                if (currentChoice < 0) // if the players currentchoice is less than 0 
                {
                    currentChoice = 5; // set current choice back to 5 
                    turnEnd = true; // and set turnend to true(display the turn end graphic)
                }

                // code to check the touching of the end turn button it then sets turnend to false calls the nextturn function and increases the turnvalue by 1
                if (turnEnd && pressed && touchsphere.Intersects(endTurn.bbox)) // checks if turnend is true as well so that you can only click it when turnend is true
                {
                    turnEnd = false;
                    nextturn();
                    turnValue += 1; 
                }

                // my code to call up the end of players turns and thus the matching of each players current values and setting the score 
                if (turnValue == 2) // turnvalue == 2 the end of both players turns
                {

                    bigTimerStart++; // start bigtimer
                    movesFinished = false; // set bool to false

                    if (bigTimerStart == animationTimer[0]) // if bigtimer is equal to animation timer then do whats below the next 6 are the same the timer is set to give each animation around 1 second to play 
                    {
                        turnbasedgame.choicevschoice(player1Choices[0], player2Choices[0], ref player1Score, ref player2Score, gtime, ref blueAnimation.state, ref redAnimation.state, ref pointUp); // check the players first set of choicies first
                        pointUp = true; // set pointup back to true so they can add points on for the next choice check as well
                    }

                    if (bigTimerStart == animationTimer[1]) // checking the second set of choices
                    {
                        turnbasedgame.choicevschoice(player1Choices[1], player2Choices[1], ref player1Score, ref player2Score, gtime, ref blueAnimation.state, ref redAnimation.state, ref pointUp);
                        pointUp = true;
                    }

                    if (bigTimerStart == animationTimer[2]) // checking the third set of choices
                    {
                        turnbasedgame.choicevschoice(player1Choices[2], player2Choices[2], ref player1Score, ref player2Score, gtime, ref blueAnimation.state, ref redAnimation.state, ref pointUp);
                        pointUp = true;
                    }

                    if (bigTimerStart == animationTimer[3]) // checking the fourth set of choices
                    {
                        turnbasedgame.choicevschoice(player1Choices[3], player2Choices[3], ref player1Score, ref player2Score, gtime, ref blueAnimation.state, ref redAnimation.state, ref pointUp);
                        pointUp = true;
                    }

                    if (bigTimerStart == animationTimer[4]) // checking the fifth set of choices
                    {
                        turnbasedgame.choicevschoice(player1Choices[4], player2Choices[4], ref player1Score, ref player2Score, gtime, ref blueAnimation.state, ref redAnimation.state, ref pointUp);
                        pointUp = true;
                    }

                    if (bigTimerStart == animationTimer[5]) // checking the sixth set of choices
                    {
                        turnbasedgame.choicevschoice(player1Choices[5], player2Choices[5], ref player1Score, ref player2Score, gtime, ref blueAnimation.state, ref redAnimation.state, ref pointUp);
                        pointUp = true;
                    }

                    if (bigTimerStart == animationTimer[6]) // this basically sets animations back to state 0 and checks if this is the last set of rounds based on how many rounds each player has won
                    {
                        blueAnimation.state = 0; // setting default player animations back
                        redAnimation.state = 0; // setting default player animations back 
 
                       if (player1RoundsWon <= 2 || player2RoundsWon <= 2) // if player 1s or player 2s rounds won are less than or equal too 2 then call next round function
                        {
                            nextround();
                        }

                        bigTimerStart = 0; // set the timer back to 0
                    }
                }

                // if not in winStreakMode (normal mode) then set the max rounds winable by either player to 3
                // and then let a player 'Win' the game upon reaching 3 rounds won.
                if (winStreakMode == false)
                {
                    if (player1RoundsWon == 3) // if player1 rounds won is == 3 player1gamewin = true
                    {
                        player1GameWin = true;
                    }

                    if (player2RoundsWon == 3) // if player2 rounds won is == 3 player2gamewin = true
                    {
                        player2GameWin = true;
                    }

                    if (player1GameWin && pressed && touchsphere.Intersects(player1WinScreen.bbox)) // if player 1 has won and the player taps on the screen gameover is set to true
                    {
                        gameOver = true;
                    }

                    if (player2GameWin && pressed && touchsphere.Intersects(player2WinScreen.bbox)) // if player 2 has won and the player taps on the screen gameover is set to true
                    {
                        gameOver = true;
                    }
                }

                if (player1RoundWinBool && pressed && touchsphere.Intersects(player1RoundWin.bbox)) // if playerroundwin bool is set to true (the image is showing) and the player clicks the image then it gets set back to false
                {
                    player1RoundWinBool = false;
                }

                if (player2RoundWinBool && pressed && touchsphere.Intersects(player2RoundWin.bbox)) // if playerroundwin bool is set to true (the image is showing) and the player clicks the image then it gets set back to false
                {
                    player2RoundWinBool = false;
                }

                if (drawBool && pressed && touchsphere.Intersects(draw.bbox)) // if drawbool is true(the players drew and its displayed on screen) and the screens clicked then set it to false
                {
                    drawBool = false;
                }

                if (winStreakMode == true)
                {
                    // this is my win streak clear code replacing my playerwingame code
                    if (player1StreakEnd) // if player 1 streak end is true 
                    {
                        player1RoundsWon = 0; // player1roundswon is set back to 0
                        player1StreakEnd = false; // and the boolean is set back to false so it doesnt continue to set the score to 0 
                    }

                    if (player2StreakEnd) // if player 2 streak end is true 
                    {
                        player2RoundsWon = 0; // player2roundswon is set back to 0
                        player2StreakEnd = false; // and the boolean is set back to false so it doesnt continue to set the score to 0 
                    }
                }

                // change the bottom highscore if either players rounds won is greater than the bottom highscore
                // it's here and not in gameOver area because we wish it to update constantly during the win streak
                // mode
                if (player1RoundsWon > highscores[9])
                {
                    highscores[9] = player1RoundsWon;
                }
                else if (player2RoundsWon > highscores[9])
                {
                    highscores[9] = player2RoundsWon;
                }
            }
            else if (gameOver)
            {
                // Game is over, allow game to return to the main menu
                      
                //SORT HIGH SCORES
                Array.Sort(highscores, highscoreNames);
                Array.Reverse(highscores);
                Array.Reverse(highscoreNames);

                // Allow user to quit game
                   gameState = -1;      
            }
        }

        public void drawgame()
        {
            // Draw the in-game graphics
            sfunctions3d.resetgraphics(GraphicsDevice);

            // if the game isn't over draw the below 
            if (player1GameWin == false && player2GameWin == false)
                ground.drawme(gameCamera, false); // draw the background
            
            for (int count = 0; count < numberOfMoves; count++) // draw the moves(the ones used as buttons)
            {
                moves[count].drawme(gameCamera, true);
            }
            
            for (int count = 0; count < numberOfChoices; count++) // draw the choices
            {
                for (int i=0; i < numberOfMoves; i++)
                    choices[count, i].drawme(gameCamera, true);
            }
            
            spriteBatch.Begin();
            
            // if the game isn't over draw the below (largely to stop screen flicker when going from game win
            // to main menu)
            if (player1GameWin == false && player2GameWin == false)
            {
                deleteButton.drawme(ref spriteBatch);                         // draw the delete button
                blueAnimation.drawme(ref spriteBatch, blueAnimation.position);   // draw animation on its current state
                redAnimation.drawme(ref spriteBatch, redAnimation.position); // draw animation2 on its current state
                exit.drawme(ref spriteBatch);                            // draw exit button
            }

            // draw endturn button if the turn has ended(turnend = true)
            if (turnEnd)                         
                endTurn.drawme(ref spriteBatch);                

            // draw the player 1 round win screen if player 1 round win boolean is true
            if (player1RoundWinBool)  
                player1RoundWin.drawme(ref spriteBatch);

            // draw the player 2 round win screen if player 2 round win boolean is true
            if (player2RoundWinBool)
                player2RoundWin.drawme(ref spriteBatch);

            // if its a draw (drawbool is true) then draw the draw screen
            if(drawBool) 
            draw.drawme(ref spriteBatch);

            // draw the player 2 win screen if player 2 win boolean is true
            if (player2GameWin)
                player2WinScreen.drawme(ref spriteBatch);

            // draw the player 1 win screen if player 1 win boolean is true
            if (player1GameWin)
                player1WinScreen.drawme(ref spriteBatch);

            // draw the rounds won and hits text to screen for each player so they can check there score
            spriteBatch.DrawString(mainFont, " Blue Rounds Won " + player1RoundsWon.ToString(), new Vector2(180, 10), Color.White, MathHelper.ToRadians(0), new Vector2(0, 0), 0.5f, SpriteEffects.None, 0);
            spriteBatch.DrawString(mainFont, " Red Rounds Won " + player2RoundsWon.ToString(), new Vector2(420, 10), Color.White, MathHelper.ToRadians(0), new Vector2(0, 0), 0.5f, SpriteEffects.None, 0);
            spriteBatch.DrawString(mainFont, " Blue Hits " + player1Score.ToString(), new Vector2(210, 60), Color.White, MathHelper.ToRadians(0), new Vector2(0, 0), 0.5f, SpriteEffects.None, 0);
            spriteBatch.DrawString(mainFont, " Red Hits " + player2Score.ToString(), new Vector2(450, 60), Color.White, MathHelper.ToRadians(0), new Vector2(0, 0), 0.5f, SpriteEffects.None, 0);
            
            spriteBatch.End();

        }


        public void updatetutorial()
        {
            // Update code for the tutorial screen

            // Allow game to return to the main menu
            if (pad[0].Buttons.Back == ButtonState.Pressed) gameState = -1;
        }

        public void drawtutorial()
        {
            // Draw graphics for OPTIONS screen
            spriteBatch.Begin();
            tutorialPage.drawme(ref spriteBatch); // drawing the tutorial page  
            spriteBatch.End();
        }

        public void updatehighscoreandoptions()
        {
            // Update code for the high score screen

            // Check for touch over various things
            TouchCollection tcoll = TouchPanel.GetState();
            Boolean pressed = false;
            BoundingSphere touchsphere = new BoundingSphere(new Vector3(0, 0, 0), 0);

            foreach (TouchLocation t1 in tcoll)
            {
                if (t1.State == TouchLocationState.Pressed || t1.State == TouchLocationState.Moved)
                {
                    pressed = true;
                    touchsphere = new BoundingSphere(new Vector3(t1.Position.X, t1.Position.Y, 0), 1);
                }
            }
     
            // if the soundon bbox is pressed then set the soundon bool to false (turn the sound off)
            if(pressed && touchsphere.Intersects(soundOn.bbox)) 
            {
                soundOnBool = false;
            }
     
            // if the soundoff bbox is pressed then set the soundon bool to true (turn the sound on)
            if (pressed && touchsphere.Intersects(soundOff.bbox))
            {
                soundOnBool = true;
            }

            // if sound on bool is true(sound is on) and music is stopped then play the music 
            if (soundOnBool)
            {
                if (music.State == SoundState.Stopped) music.Play();
            }
            else if (music.State == SoundState.Playing) music.Stop(); // else if its playing and soundonbool is not true stop it

            // Allow game to return to the main menu
            if (pad[0].Buttons.Back == ButtonState.Pressed) gameState = -1;

        }

        public void drawhighscoreandoptions()
        {
            // Draw graphics for High Score table
            spriteBatch.Begin();
            regularBg.drawme(ref spriteBatch);

            // if sound on bool is true draw the soundon 2d sprite
            if (soundOnBool)
            {
                soundOn.drawme(ref spriteBatch);
            }

            // if sound on bool is false draw the soundoff 2d sprite
            if (!soundOnBool)
            {
                soundOff.drawme(ref spriteBatch);
            }

            // Draw top ten high scores
            for (int i = 0; i < numberOfHighscores; i++)
            {
                spriteBatch.DrawString(mainFont, (i + 1).ToString("0") + ". " + highscoreNames[i], new Vector2(180, 100 + (i * 34)),
                    Color.White, MathHelper.ToRadians(0), new Vector2(0, 0), 1.2f, SpriteEffects.None, 0);
                spriteBatch.DrawString(mainFont, highscores[i].ToString("0"), new Vector2(displayWidth / 2 + 80, 100 + (i * 34)),
                   Color.White, MathHelper.ToRadians(0), new Vector2(0, 0), 1.2f, SpriteEffects.None, 0);
            }

            title[1].drawme(ref spriteBatch); // draw the title for this gamestate

            spriteBatch.End();
        }



        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            // Draw stuff depending on the game state
            switch (gameState)
            {
                case -1:
                    // Game is on the main menu
                    drawmenu();
                    break;
                case 0:
                    // Game is being played
                    drawgame();
                    break;
                case 1:
                    // Options menu
                    drawtutorial();
                    break;
                case 2:
                    // High Score table
                    drawhighscoreandoptions();
                    break;

                case 3:
                    //Draw Game Mode 2
                    drawgame();
                    break;

                default:
                    break;
            }

            base.Draw(gameTime);
        }
    }
}
