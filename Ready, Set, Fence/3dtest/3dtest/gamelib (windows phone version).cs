using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch; // Include for Windows Phone games
using Microsoft.Xna.Framework.Media;
using System.IO;

namespace gamelib
{
    // Class for 2D graphics
    public class graphic2d
    {
        private Texture2D image;                 // Texture to hold image
        public Rectangle rect;                  // Rectangle to hold position & size of the image

        public graphic2d() { }  // Empty constructor to avoid crashes

        // Constructor which loads image and fits the background to fill the width of the screen
        public graphic2d(ContentManager content, string spritename, int dwidth, int dheight)
        {
            image = content.Load<Texture2D>(spritename);
            float ratio = ((float)dwidth / image.Width);
            rect.Width = dwidth;
            rect.Height = (int)(image.Height * ratio);
            rect.X = 0;
            rect.Y = (dheight - rect.Height) / 2;
        }
        
        // Load a jpeg or png from a file not in the content pipeline
        public graphic2d(GraphicsDeviceManager graphics, ContentManager content, string spritename, int dwidth, int dheight)
        {
            Stream picstream = File.Open(spritename, FileMode.Open);
            image = Texture2D.FromStream(graphics.GraphicsDevice, picstream); // Load an image from the HDD not in the content pipeline
            picstream.Close();
            float ratio = ((float)dwidth / image.Width);    // Work out the ratio for the image depending on screen size
            rect.Width = dwidth;                            // Set image width to match the screen width
            rect.Height = (int)(image.Height * ratio);      // Work out new height based on the screen aspect ratio
            rect.X = 0;
            rect.Y = (dheight - rect.Height) / 2;           // Put image in the middle of the screen on the Y axis
        }

        // Use this method to draw the image
        public void drawme(ref SpriteBatch spriteBatch2)
        {
            spriteBatch2.Draw(image, rect, Color.White);
        }
    }


    // Class for 2D sprites
    public class sprite2d
    {
        public Texture2D image;         		// Texture which holds image
        public Vector3 position; 		 	    // Position on screen
        public Rectangle rect;          		// Rectangle to hold size and position
        public Vector2 origin;          		// Centre point
        public float rotation = 0;          	// Amount of rotation to apply
        public float rotspeed = 0.05f;          // Speed they should spin at
        public Vector3 velocity;        		// Velocity (Direction and speed)
        public BoundingSphere bsphere;  		// Bounding sphere
        public BoundingBox bbox;                // Bounding box
        public Boolean visible = true;    		// Should object be drawn true or false
        public Color colour = Color.White;      // Holds colour to draw the image in
        public float size;                      // Size ratio of object

        public sprite2d() { }                   // Empty constructor to avoid crashes

        // Constructor which initialises the sprite2D
        public sprite2d(ContentManager content, string spritename, int x, int y, float msize, Color mcolour, Boolean mvis)
        {
            image = content.Load<Texture2D>(spritename);    // Load image into texture
            position = new Vector3((float)x, (float)y, 0);  // Set position
            rect.X = x;                                     // Set position of draw rectangle x
            rect.Y = y;                                     // Set position of draw rectangle y
            origin.X = image.Width / 2;               	    // Set X origin to half of width
            origin.Y = image.Height / 2;              	    // Set Y origin to half of height
            rect.Width = (int)(image.Width * msize);  	    // Set the new width based on the size ratio 
            rect.Height = (int)(image.Height * msize);	    // Set the new height based on the size ratio
            colour = mcolour;                               // Set colour
            visible = mvis;                                 // Image visible TRUE of FALSE? 
            size = msize;                                   // Store size ratio

            updateobject();
        }

        public void updateobject()
        {
            // Set position of object into the rectangle from the position Vector2
            rect.X = (int)position.X;
            rect.Y = (int)position.Y;
            // Create Boundingsphere around the object
            bsphere = new BoundingSphere(position, rect.Width / 2);
            // Create Boundingbox around the object
            bbox = new BoundingBox(new Vector3(position.X - rect.Width / 2, position.Y - rect.Height / 2, 0),
                                    new Vector3(position.X + rect.Width / 2, position.Y + rect.Height / 2, 0));
        }

        // Use this method to draw the image
        public void drawme(ref SpriteBatch sbatch)
        {
            if (visible)
                sbatch.Draw(image, rect, null, colour, rotation, origin, SpriteEffects.None, 0);
        }

        // Use this method to draw the image at a specified position
        public void drawme(ref SpriteBatch sbatch, Vector3 newpos)
        {
            if (visible)
            {
                Rectangle newrect = rect;
                newrect.X = (int)newpos.X;
                newrect.Y = (int)newpos.Y;

                sbatch.Draw(image, newrect, null, colour, rotation, origin, SpriteEffects.None, 0);
            }
        }
    }

    // Class for 2D animation
    public class animation
    {
        private Texture2D image;            // Texture which holds animation sheet
        public Vector3 position;    // Position of animation
        public Rectangle rect;      // Rectangle to hold size and position
        private Rectangle frame_rect;       // Rectangle to hold position of frame to draw
        private Vector2 origin;             // Centre point
        public float rotation = 0;  // Rotation amount
        public Color colour = Color.White; // Colour
        public float size;          // Size Ratio
        public Boolean visible;     // Should object be drawn true or false
        public int framespersecond; // Frame Rate
        private int frames;                 // Number of frames of animation
        private int rows;                   // Number of rows in the sprite sheet
        private int columns;                // Number of columns in the sprite sheet
        private int frameposition;          // Current position in the animation
        private int framewidth;             // Width in pixels of each frame of animation
        private int frameheight;            // Height in pixels of each frame of animation
        private float timegone;             // Time since animation began
        private Boolean loop = false;// Should animation loop
        private int noofloops = 0;          // Number of loops to do
        private int loopsdone = 0;          // Number of loops completed
        public Boolean paused = false;  // Freeze frame animation
        private Boolean playbackwards = false;  // Sets whether animation should play forwards or backwards

        public animation() { }

        // Constructor which initialises the animation
        public animation(ContentManager content, string spritename, int x, int y, float msize, Color mcolour, Boolean mvis, int fps, int nrows, int ncol, Boolean loopit, Boolean playback)
        {
            image = content.Load<Texture2D>(spritename);    // Load image into texture
            position = new Vector3((float)x, (float)y, 0);  // Set position
            rect.X = x;                                     // Set position of draw rectangle x
            rect.Y = y;                                     // Set position of draw rectangle y
            size = msize;                                   // Store size ratio
            colour = mcolour;                               // Set colour
            visible = mvis;                                 // Image visible TRUE of FALSE? 
            framespersecond = fps;                          // Store frames per second
            rows = nrows;                                   // Number of rows in the sprite sheet
            columns = ncol;                                 // Number of columns in the sprite sheet
            frames = rows * columns;                          // Store no of frames
            framewidth = (int)(image.Width / columns);      // Calculate the width of each frame of animation
            frameheight = (int)(image.Height / rows);       // Calculate the heigh of each frame of animation
            rect.Width = (int)(framewidth * size);          // Set the new width based on the size ratio    
            rect.Height = (int)(frameheight * size);	    // Set the new height based on the size ratio
            frame_rect.Width = framewidth;                  // Set the width of each frame
            frame_rect.Height = frameheight;                // Set the height of each frame
            origin.X = framewidth / 2;                      // Set X origin to half of frame width
            origin.Y = frameheight / 2;              	    // Set Y origin to half of frame heigh
            loop = loopit;                                  // Should it be looped or not
            playbackwards = playback;                       // Should animation be played forwards or backwards
        }   

        public void start(Vector3 pos, float rot, int repeatnumber)
        {
            // Set position of object into the rectangle from the position Vector
            position = pos;
            rect.X = (int)position.X;
            rect.Y = (int)position.Y;

            // Start new animation
            noofloops = repeatnumber;
            rotation = rot;
            visible = true;
            frameposition = 0;
            timegone = 0;
            loopsdone = 0;
            paused = false;
         }

        public void update(float gtime)
        {
            if (framespersecond < 1) framespersecond = 1; // Error checking to avoid divide by zero

            if (visible && !paused)
            {
                frameposition = (int)(timegone / (1000 / framespersecond));   // Work out what frame the animation is on

                // Inverse frame position number if you want to play the animation backwards
                if (playbackwards)
                    frameposition = (frames - 1) - frameposition;

                timegone += gtime;                                          // Time gone during the animation
                // Check if the animation is at the end
                if ((!playbackwards && frameposition >= frames) || (playbackwards && frameposition < 0))
                {
                    // Repeat animation if necessary
                    if (loop || loopsdone < noofloops)
                    {
                        loopsdone++;
                        if (!playbackwards)
                            frameposition = 0;
                        else
                            frameposition = frames-1;
                        timegone = 0;
                    }
                    else
                    {
                        visible = false;   // End animation
                    }
                }
            }
        }

        // Use this method to draw the image
        public void drawme(ref SpriteBatch sbatch)
        {
            if (visible)
            {   // Work out the co-ordinates of the current frame and then draw that frame
                frame_rect.Y = ((int)(frameposition / columns)) * frameheight;
                frame_rect.X = (frameposition - ((int)(frameposition / columns)) * columns) * framewidth;
                sbatch.Draw(image, rect, frame_rect, colour, rotation, origin, SpriteEffects.None, 0);
            }
        }

        // Use this method to draw the image at a specified position
        public void drawme(ref SpriteBatch sbatch, Vector3 newpos)
        {
            if (visible)
            {
                Rectangle newrect = rect;
                newrect.X = (int)newpos.X;
                newrect.Y = (int)newpos.Y;

                frame_rect.Y = ((int)(frameposition / columns)) * frameheight;
                frame_rect.X = (frameposition - ((int)(frameposition / columns)) * columns) * framewidth;
                sbatch.Draw(image, newrect, frame_rect, colour, rotation, origin, SpriteEffects.None, 0);
            }
        }

        // Use this method to draw the image at a specified position and allow image to be flipped horizontally or vertically
        public void drawme(ref SpriteBatch sbatch, Vector3 newpos, Boolean flipx, Boolean flipy)
        {
            if (visible)
            {
                Rectangle newrect = rect;
                newrect.X = (int)newpos.X;
                newrect.Y = (int)newpos.Y;

                frame_rect.Y = ((int)(frameposition / columns)) * frameheight;
                frame_rect.X = (frameposition - ((int)(frameposition / columns)) * columns) * framewidth;
                if (flipx)
                    sbatch.Draw(image, newrect, frame_rect, colour, rotation, origin, SpriteEffects.FlipHorizontally, 0);
                else if (flipy)
                    sbatch.Draw(image, newrect, frame_rect, colour, rotation, origin, SpriteEffects.FlipVertically, 0);
                else
                    sbatch.Draw(image, newrect, frame_rect, colour, rotation, origin, SpriteEffects.None, 0);
            }
        }
    }


    // Class for animated 2D moving sprite, animatedsprite also acts as a storage class for a number of sprite animations.
    public class animatedsprite
    {
        public animation[] spriteanimation;     // Holds the animations for the sprite
        public Vector3 position; 		 	    // World position
        public Vector3 screenposition;          // Position on the screen
        public Boolean visible = true;    		// Should object be drawn true or false
        public int state = 0;                   // State that guy is in
        private int numberofstates = 1;         // Number of different states (animations) that the guy can be in
        private int oldstate = -1;              // Previous state
        private int previousstate = -1;          // Old state
        public float rotation = 0;              // Rotation amount

        public animatedsprite() { }             // Empty constructor to avoid crashes

        // Constructor to set initial position and old position
        public animatedsprite(Vector3 pos, int numberofanimations)
        {
            numberofstates = numberofanimations;
            spriteanimation = new animation[numberofstates];
            position = pos;             // Set current position of character
        }

        // Update the current sprite to update
        public void updatesprite(float gtime)
        {
            if (visible)
            {
                // If state has changed start the new animation
                if (oldstate != state)
                {
                    previousstate = oldstate;
                    spriteanimation[state].start(screenposition, rotation, 0);   // Start the animation and set it to LOOP
                }

                // update the animation
                spriteanimation[state].update(gtime);

                oldstate = state; // Store current state

                // If an animation has finished playing reset it to the previous animation
                if (!spriteanimation[state].visible && previousstate != state && previousstate >= 0)
                {
                    state = previousstate;
                    spriteanimation[state].start(screenposition, rotation, 0);   // Start the animation
                }
            }
        }

        // Draw the correct sprite animation at the current position 
        public void drawme(ref SpriteBatch sbatch)
        {
            if (visible)
                spriteanimation[state].drawme(ref sbatch, screenposition);
        }
        
        // Use this method to draw the sprite animation at a specified position
        public void drawme(ref SpriteBatch sbatch, Vector3 newpos)
        {
            if (visible)
                spriteanimation[state].drawme(ref sbatch, newpos);
        }

    }

    public class camera
    {
        public Vector3 position;
        public Vector3 lookAt;
        public float aspectratio;
        public float fov;
        public Vector2 ViewAreaAtFarPlane;
        public Vector2 ViewAreaAtFocalPlane;
        public Vector2 ViewAreaAtNearPlane;
        public Vector3 WhichWayIsUp;
        private float nearplane;
        private float farplane;
        private float focalplane;

        public Matrix getview()
        {
            return Matrix.CreateLookAt(position, lookAt, WhichWayIsUp);    // Set the position of the camera and tell it what to look at
        }

        public Matrix getproject()
        {
            return Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(fov), aspectratio, nearPlane, farPlane);
        }

        public float farPlane
        {
            set
            {
                farplane = value;
                ViewAreaAtFarPlane = ViewAreaAt(farplane);
            }

            get
            {
                return farplane;
            }
        }

        public float focalPlane
        {
            set
            {
                focalplane = value;
                ViewAreaAtFocalPlane = ViewAreaAt(focalplane);
            }

            get
            {
                return focalplane;
            }
        }

        public float nearPlane
        {
            set
            {
                nearplane = value;
                ViewAreaAtNearPlane = ViewAreaAt(nearplane);
            }

            get
            {
                return nearplane;
            }
        }

        public Vector2 ViewAreaAt(float distance)
        {
            Vector2 area = new Vector2(0, 0);
            area.Y = (int)(distance * Math.Tan(MathHelper.ToRadians(fov / 2)));
            area.X = area.Y * aspectratio;
            return area;
        }

        public Matrix FOVMatrix()
        {
            return Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(fov), aspectratio, nearplane, farplane);
        }

        public Matrix LookMatrix()
        {
            return Matrix.CreateLookAt(position, lookAt, WhichWayIsUp);
        }

        public camera() { }

        public camera(Vector3 initialPosition, Vector3 lookat, float w_width, float w_height, int FOV, Vector3 camorient, float cameradistance, float farplanedistance)
        {
            position = initialPosition;
            lookAt = lookat;
            aspectratio = w_width / w_height;
            fov = FOV;
            WhichWayIsUp = camorient;
            nearPlane = 1f;
            farPlane = farplanedistance;
            focalPlane = cameradistance;
        }
    }


    // Class for 3D models that don't move
    public class staticmesh
    {
        public Model graphic;           // The 3D Model object that we are going to display.
        public Matrix[] transforms;     // Holds model transformation matrix
        public Vector3 rotation;        // Amount of rotation to apply on x,y and z
        public Vector3 position;        // Position on screen
        public float size;              // Size ratio (scale) 
        public float radius;            // Radius of 3D model
        public BoundingSphere bsphere;  // Bounding sphere
        public BoundingBox bbox;        // Bounding box
        public Vector3 bboxsize = new Vector3(0, 0, 0);        // Bounding box size
        public Boolean visible = true;  // Should object be drawn?
        public int value; // used to assign values to models

        public staticmesh() { }         // Empty constructor which is only in to stop crashes 

        // Constructor which loads 3D model and sets it up with size, position and initial rotation
        public staticmesh(ContentManager content, string modelname, float msize, Vector3 mpos, Vector3 mrot)
        {
            graphic = content.Load<Model>(modelname);                   // Load the 3D model from the ContentManager
            transforms = new Matrix[graphic.Bones.Count];               // make an array of transforms, one for each 'bone' in the 3d model
            graphic.CopyAbsoluteBoneTransformsTo(transforms);           // copy the transforms from the 3d model into this array ready for use
            size = msize;                                               // Set size 
            radius = graphic.Meshes[0].BoundingSphere.Radius * size;    // Work out the radius 
            position = mpos;                                            // Set initial position
            rotation = mrot;                                            // Set intial rotation 
            updateobject();
        }

        public void drawme(camera newcam, Boolean lightson)
        {
            if (visible)
                sfunctions3d.drawmesh(position, rotation, size, graphic, transforms, lightson, newcam);
        }

        public void updateobject()
        {
            if (bboxsize.X == 0)
            {
                // Create a boundingsphere around object
                bsphere = new BoundingSphere(position, radius);
            }
            else
            {
                // Create a bounding box around the 3D model
                Vector3 leftcorner = position;
                Vector3 rightcorner = position;
                leftcorner -= bboxsize;
                rightcorner += bboxsize;
                bbox = new BoundingBox(leftcorner, rightcorner);
            }
        }
    }

    public static class sfunctions3d
    {
        // Reset graphics device for 3D drawing
        public static void resetgraphics(GraphicsDevice graphics)
        {
            // These lines reset the graphics device for drawing 3D
            graphics.BlendState = BlendState.Opaque;
            graphics.DepthStencilState = DepthStencilState.Default;
            graphics.SamplerStates[0] = SamplerState.LinearWrap;
        }

        // This method draws a 3D model
        public static void drawmesh(Vector3 position, Vector3 rotation, float scale, Model graphic, Matrix[] transforms, Boolean lightson, camera newcamera)
        {
            foreach (ModelMesh mesh in graphic.Meshes)               // loop through the mesh in the 3d model, drawing each one in turn.
            {
                foreach (BasicEffect effect in mesh.Effects)                // This loop then goes through every effect in each mesh.
                {

                    if (lightson) effect.EnableDefaultLighting();           // Enables default lighting when lightson==TRUE, this can do funny things with textured on 3D models.
                    effect.PreferPerPixelLighting = true;                   // Makes it shiner and reflects light better
                    effect.World = transforms[mesh.ParentBone.Index];       // begin dealing with transforms to render the object into the game world
                    effect.World *= Matrix.CreateScale(scale);              // scale the mesh to the right size
                    effect.World *= Matrix.CreateRotationX(rotation.X);     // rotate the mesh
                    effect.World *= Matrix.CreateRotationY(rotation.Y);     // rotate the mesh
                    effect.World *= Matrix.CreateRotationZ(rotation.Z);     // rotate the mesh
                    effect.World *= Matrix.CreateTranslation(position);     // position the mesh in the game world

                    effect.View = Matrix.CreateLookAt(newcamera.position, newcamera.lookAt, newcamera.WhichWayIsUp);    // Set the position of the camera and tell it what to look at

                    // Sets the FOV (Field of View) of the camera. The first paramter is the angle for the FOV, the 2nd is the aspect ratio of your game, 
                    // the 3rd is the nearplane distance from the camera and the last paramter is the farplane distance from the camera.
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(newcamera.fov), newcamera.aspectratio, newcamera.nearPlane, newcamera.farPlane);
                }
                mesh.Draw(); // draw the current mesh using the effects.
            }
        }

    }

    // I am aware that this function would need to be modified a bit more so as to be transferrable to another game/program however I wanted to put it in here for tidyness.
    public static class turnbasedgame
    {
        public static void choicevschoice(int p1choice, int p2choice, ref int p1score, ref int p2score, float gtime, ref int p1animation, ref int p2animation, ref Boolean pointavailable)
        {
            // this is the function I created to match up all of my choices I will not go into detail on every single I will only write up the one ones that are different as there is 25 different outcomes and they are displayed on my tutorial

            if (p1choice == 1 && p2choice == 1) // this matches up the choices so if the values are both 1 then play animation state 1 for both characters ( this is a draw as no points are awarded)
            {
                p1animation = 1;
                p2animation = 1;
            }

            if (p1choice == 2 && p2choice == 2)
            {
                p1animation = 3;
                p2animation = 3;
            }

            if (p1choice == 3 && p2choice == 3)
            {
                p1animation = 2;
                p2animation = 2;
            }

            if (p1choice == 4 && p2choice == 4)
            {
                p1animation = 5;
                p2animation = 5;
            }

            if (p1choice == 5 && p2choice == 5)
            {
                p1animation = 4;
                p2animation = 4;
            }

            if (p1choice == 1 && p2choice == 4) // parry vs upward slash no points given as it counts as a block also plays the correct animation states
            {
                p1animation = 1;
                p2animation = 5;
            }

            if (p1choice == 4 && p2choice == 1) // parry vs upward slash no points given as its blocked 
            {
                p1animation = 5;
                p2animation = 1;
            }

            if (p1choice == 1 && p2choice == 3) // parry vs thrust no points given as its blocked 
            {
                p1animation = 1;
                p2animation = 2;
            }

            if (p1choice == 3 && p2choice == 1) // parry vs thrust no points given as its blocked
            {
                p1animation = 2;
                p2animation = 1;
            }

            if (p1choice == 2 && p2choice == 1) // dodge vs parry no points given as its blocked 
            {
                p1animation = 3;
                p2animation = 1;
            }

            if (p1choice == 1 && p2choice == 2) // parry vs dodge no points given as its blocked 
            {
                p1animation = 1;
                p2animation = 3;
            }

            if (p1choice == 2 && p2choice == 5) // dodge vs downward slash no points given as its blocked 
            {
                p1animation = 3;
                p2animation = 4;
            }

            if (p1choice == 5 && p2choice == 2) // dodge vs downward slash no points given as its blocked 
            {
                p1animation = 4;
                p2animation = 3;
            }

            if (pointavailable && p1choice == 3 && p2choice == 2) // thrust vs dodge, thrust wins, point given if points boolean is true 
            {
                p1animation = 2;
                p2animation = 3;
                p1score += 1; // p1 score goes up in increments of 1 
                pointavailable = false; // sets pointup to false so the points stop going up 
            }

            if (pointavailable && p1choice == 2 && p2choice == 3) // dodge vs thrust, thrust wins, point given
            {
                p1animation = 3;
                p2animation = 2;
                p2score += 1; // passed in int goes up in increments of 1 
                pointavailable = false; // sets passed in boolean to false so the points stop going up
            }

            if (pointavailable && p1choice == 4 && p2choice == 3) // upward slash vs thrust, thrust wins, point given
            {
                p1animation = 5;
                p2animation = 2;
                p2score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 3 && p2choice == 4) // upward slash vs thrust, thrust wins, point given
            {
                p1animation = 2;
                p2animation = 5;
                p1score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 3 && p2choice == 5) // downwardslash vs thrust, downwardslash wins, point given
            {
                p1animation = 2;
                p2animation = 4;
                p2score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 5 && p2choice == 3) // downwardslash vs thrust, downwardslash wins, point given
            {
                p1animation = 4;
                p2animation = 2;
                p1score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 1 && p2choice == 5) // downwardslash vs parry, downwardslash wins, point given
            {
                p1animation = 1;
                p2animation = 4;
                p2score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 5 && p2choice == 1) // downward slash vs parry, downwardslash wins, point given
            {
                p1animation = 4;
                p2animation = 1;
                p1score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 4 && p2choice == 5) // upward slash vs  downward slash, upwardslash wins, point given
            {
                p1animation = 5;
                p2animation = 4;
                p1score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 5 && p2choice == 4) // upward slash vs  downward slash, upwardslash wins, point given
            {
                p1animation = 4;
                p2animation = 5;
                p2score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 2 && p2choice == 4) // upward slash vs dodge, upwardslash wins, point given
            {
                p1animation = 3;
                p2animation = 5;
                p2score += 1;
                pointavailable = false;
            }

            if (pointavailable && p1choice == 4 && p2choice == 2) // upward slash vs dodge, upwardslash wins, point given
            {
                p1animation = 5;
                p2animation = 3;
                p1score += 1;
                pointavailable = false;
            }
        }
    }
}